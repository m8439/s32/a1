
const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const cors = require('cors');

const PORT = 3007;
const app = express();

//connect our routes module

const userRoutes = require ('./routes/userRoutes');

//middleware to handle json payloads

app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors()) //prevents blocking of request from clints esp different domains

//connect  Database to server
mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});

//test DB connection
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to Database`));

//Schema
	//Schema is in models module


//Routes
	//create a middleware to be the root url of all routes
app.use(`/api/users`, userRoutes);


app.listen(PORT, () => console.log(`Server connected to port ${PORT}`))